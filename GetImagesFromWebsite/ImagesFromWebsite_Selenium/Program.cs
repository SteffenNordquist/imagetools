﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.PhantomJS;
using ProcuctDB;
using ProcuctDB.Boltze;
using ProcuctDB.Haba;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ImagesFromWebsite_Selenium
{
    public class Program
    {
        static void Main(string[] args)
        {

            List<IDatabaseLoginFromWebsite> dbList = new List<IDatabaseLoginFromWebsite>();
            //dbList.Add(new BoltzeDB());
           

            ParallelOptions po = new ParallelOptions();
            po.MaxDegreeOfParallelism = 4;

            Parallel.ForEach(dbList, po, database =>
            {
                var items = database.GetProductsToUpdate();

                Parallel.ForEach(items, po, item =>
                {
                    string ean2 = item.GetEanList().First();
                    string websitelink = "";
                    object lockThis = new object();

                    var driverService = PhantomJSDriverService.CreateDefaultService();
                    driverService.HideCommandPromptWindow = true;
                    var driver = new PhantomJSDriver(driverService);

                    try
                    {

                        var link = item.getLink();
                        


                        if (link != null)
                        {

                           
                            
                            driver.Navigate().GoToUrl(item.getWebsiteLoginPage());
                            

                            if (CheckIfAlreadyLogin(driver) == false)
                            {
                                IWebElement accNode = driver.FindElement(By.XPath(item.getUserNameNode()));
                                accNode.SendKeys(item.getUserName());
                                IWebElement passNode = driver.FindElement(By.XPath(item.getPasswordNode()));
                                passNode.SendKeys(item.getPassword());

                                IWebElement passSubmit = driver.FindElement(By.XPath(item.getSubmitButtonNode()));
                                passSubmit.Click();

                                driver.Navigate().GoToUrl(link);

                                try
                                {
                                    lock (lockThis)
                                    {


                                        var imageNodes = driver.FindElements(By.XPath(item.getImageNode()));
                                        string imageAttribute = item.GetImageAttribute();
                                        int counter = 1;


                                        foreach (var imageNode in imageNodes)
                                        {
                                            string suppliername = item.getSupplierName();
                                            string ean = item.GetEanList().First();
                                            string articlenumber = item.getArticleNumber();
                                            ean2 = ean;


                                            string imgSrc = driver.FindElement(By.XPath(item.getImageNode())).GetAttribute(imageAttribute);



                                            if (!imgSrc.Contains("http"))
                                            {
                                                websitelink = item.getWebsite();
                                                imgSrc = websitelink + imgSrc;
                                            }


                                            database.UpdateItemImage(ean, counter, articlenumber);

                                            DownloadImage(suppliername, imgSrc, ean + "_" + counter);
                                            counter++;
                                        }
                                       
                                        Console.WriteLine(database.GetType().Name + " " + ean2 + " images downloaded and SAVED");
                                    }

                                }
                                catch
                                {

                                }


                            }

                        }
                        else
                        {
                            Console.WriteLine(item.GetType().Name + " " + ean2 + " no link " );
                        }
                    }
                    catch
                    {

                       
                        Console.WriteLine(item.GetType().Name + " " + ean2 + " ERROR ");

                    }

                    driver.Close();
                    driver.Dispose();

                });

            });


        }





        public static void DownloadImage(string suppliername, string imagelink, string ean)
        {

            // automatically create a folder to desktop
            // downloads and saves the image to desktop with suppliername_images as filename


            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string filename = "\\" + suppliername.ToLower() + "\\";
            string filepath = path + filename;
            Image image = Image.FromStream(new MemoryStream(new WebClient().DownloadData(imagelink)));

            if (!Directory.Exists(filepath))
            {
                Directory.CreateDirectory(filepath);
            }

            //if (File.Exists(filepath + ean + ".jpg"))
            //{
            //    string datetime  = DateTime.Now.ToString();

            //    image.Save(filepath + ean + "heloo" + ".jpg");
            //}
            //else
            //{

            image.Save(filepath + ean + ".jpg");
            //}
        }


        public static bool CheckIfAlreadyLogin(IWebDriver driver)
        {
            try
            {
                driver.FindElement(By.CssSelector(".logoutbutton"));
                return true; //if found
            }

            catch //if no logoutbutton element found
            {
                return false;
            }
        }



    }
}
