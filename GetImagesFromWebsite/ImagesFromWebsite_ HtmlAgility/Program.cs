﻿using HtmlAgilityPack;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ProcuctDB;
using ProcuctDB.Clementoni;
using ProcuctDB.Fischer;
using ProcuctDB.Gallay;
using ProcuctDB.Gardena;
using ProcuctDB.Goki;
using ProcuctDB.Haba;
using ProcuctDB.Herweck;
using ProcuctDB.Jpc;
using ProcuctDB.Loqi;
using ProcuctDB.Moleskine;
using ProcuctDB.Primavera;
using ProcuctDB.Ravensburger;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ImagesFromWebsite_HtmlAgility
{
    public class Program
    {
        public static MongoClient Client;
        public static MongoServer Server;
        public static MongoDatabase PDatabase;
        public static MongoCollection<JpcEntity> mongoCollection;

        static void Main(string[] args)
        {
            Client = new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/PD_JPC");
            Server = Client.GetServer();
            PDatabase = Server.GetDatabase("PD_JPC");
            mongoCollection = PDatabase.GetCollection<JpcEntity>("jpc");
            

            List<IDatabaseForUpdateFromWebsite> dbList = new List<IDatabaseForUpdateFromWebsite>();
            //dbList.Add(new LoqiDB());
            //dbList.Add(new KosmosDB());
            //dbList.Add(new MosesDB());
            //dbList.Add(new VTechDB());
            //dbList.Add(new SchmidtDB());
            //dbList.Add(new RavensburgerDB());
            //dbList.Add(new NorisSpieleDB());
            dbList.Add(new JpcDB());
            //dbList.Add(new PrimaveraDB());
            //dbList.Add(new SoundOfSpiritDB());
            //dbList.Add(new ClementoniDB());
            //dbList.Add(new GallayDB());
            //dbList.Add(new HabaDB());
            //dbList.Add(new GokiDB());
            //next-------
            //dbList.Add(new HerweckDB()); //done
            //beicko
            //dbList.Add(new FischerDB());
            //dbList.Add(new MoleskineDB());
            //dbList.Add(new GardenaDB());


            ParallelOptions po = new ParallelOptions();
            po.MaxDegreeOfParallelism = 1 ;

            Parallel.ForEach(dbList, po, database =>
            {

                var items = database.GetProductsToUpdate();

                //for testing 
                //var items = mongoCollection.Find(Query.EQ("ean", "0762247408420"));

                Parallel.ForEach(items, po, item =>
                {

                    //if (database.GetType() == typeof(GallayDB))
                    //{

                    //    GallayDB gallay = new GallayDB();
                    //    GetGallyPics(item, gallay);

                    //}
                    //else
                    //{
                        string ean2 = "";
                        string imageAttribute = "";
                        string websitelink = "";
                        //string imgSrc = "";


                        try
                        {
                            string itemlink = item.getLink();
                            WebClient webClient = new WebClient();

                            webClient.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");

                            //if (database.GetType() == typeof(RavensburgerDB))
                            //{
                            //    webClient.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
                            //}
                            string pageSource = webClient.DownloadString(itemlink);
                            HtmlDocument doc = new HtmlDocument();

                            doc.LoadHtml(pageSource);


                            string itemnode = item.getImageNode();

                            var imageNodes = doc.DocumentNode.SelectNodes(itemnode);

                            imageAttribute = item.GetImageAttribute();

                            int counter = 1;


                            foreach (var imageNode in imageNodes)
                            {
                                string suppliername = item.getSupplierName();
                                string ean = item.GetEanList().First();
                                string articlenumber = item.getArticleNumber();
                                ean2 = ean;
                                // edit here for nodes

                                //if (database.GetType() == typeof(KosmosDB))
                                //{

                                string imgSrc = doc.DocumentNode.SelectSingleNode(imageNode.XPath).Attributes[imageAttribute].Value;

                                //**********this line FOR LOQI ONLY**********
                                //imgSrc = "http:" + imgSrc;
                                //*******************************************


                                //string size = imgSrc.Split('/').Last();
                                //imgSrc = imgSrc.Replace(size, "739x554.jpg");

                                //}

                                //if (!imgSrc.Contains("http"))
                                //{
                                //    websitelink = item.getWebsite();
                                //    imgSrc = websitelink + imgSrc;
                                //}

                                //if (database.GetType() == typeof(ClementoniDB))
                                //{

                                //    imgSrc = imgSrc.Replace("thumbs/", "");

                                //}
                                //if (database.GetType() == typeof(HabaDB))
                                //{

                                //    imgSrc = imgSrc.Replace("javascript:changeImg('detailBild', '", "").Replace("')", "");

                                //}

                                if(DownloadImage(suppliername, imgSrc, ean + "_" + counter))
                                {
                                    database.UpdateItemImage(ean, counter, articlenumber);

                                    counter++;
                                }
                               
                            }
                            Console.WriteLine(database.GetType().Name + " " + ean2 + " images downloaded and SAVED");
                        }
                        catch (Exception ex)
                        {

                            Console.WriteLine(database.GetType().Name + " " + ean2 + " Link not found");

                        }
                    //}


                });
            });

        }

        private static void GetGallyPics(IProductUpdateFromWebsite item, GallayDB database)
        {
            List<string> imageslink = database.getImageUrls(item.GetEanList().First());
            string suppliername = database.GetType().Name;
            string ean = item.GetEanList().First();
            int count = 1;
            foreach (var imagelink in imageslink) {

                
            DownloadImage(suppliername,imagelink,ean+"_"+count);
            database.UpdateItemImage(ean, count, "");
            count++;
            }

            Console.WriteLine(database.GetType().Name + " " + ean + " images downloaded and SAVED");
        }


        public static bool DownloadImage(string suppliername, string imagelink, string ean)
        {

            // automatically create a folder to desktop
            // downloads and saves the image to desktop with suppliername_images as filename
            bool downloaded = true;
            try
            {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                string filename = "\\" + suppliername.ToLower() + "\\";
                string filepath = path + filename;
                string imagepath = filepath + ean + ".jpg";
                Image image = Image.FromStream(new MemoryStream(new WebClient().DownloadData(imagelink)));
                // image.Save(imagepath);
                // image.Dispose();
                if (!Directory.Exists(filepath))
                {
                    Directory.CreateDirectory(filepath);
                }

                //Cropper cropper = new Cropper(imagepath, imagepath);

                //if (File.Exists(filepath + ean + ".jpg"))
                //{
                //    string datetime  = DateTime.Now.ToString();

                //    image.Save(filepath + ean + "heloo" + ".jpg");
                //}
                //else
                //{

                image.Save(filepath + ean + ".jpg");
                image.Dispose();
                //}
            }
            
            catch(Exception msg)
            {
                downloaded = false;
                Console.WriteLine(msg);
            }

            return downloaded;
        }
    }
}
