﻿using HtmlAgilityPack;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using ProcuctDB;
using ProcuctDB.Boltze;
using ProcuctDB.Jpc;
using ProcuctDB.Kosmos;
using ProcuctDB.Moses;
using ProcuctDB.Ravensburger;
using ProcuctDB.Schmidt;
using ProcuctDB.SoundOfSpirit;
using ProcuctDB.VTech;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using ImagesFromWebsite_HtmlAgility;
using ImagesFromWebsite_Selenium;

namespace ImagesFromWebsite
{
    class Program
    {

        static void Main(string[] args)
        {
            string htmlagility = Assembly.GetAssembly(typeof(ImagesFromWebsite_HtmlAgility.Program)).Location;
            string selenium = Assembly.GetAssembly(typeof(ImagesFromWebsite_Selenium.Program)).Location;

            Console.WriteLine("Extracting Images");
            Process.Start(htmlagility);
           //Process.Start(selenium);


        }

    }
}
