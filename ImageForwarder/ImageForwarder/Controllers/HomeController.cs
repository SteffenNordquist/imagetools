﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ImageForwarder.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        //i => imagepath
        public ActionResult ShowImage(string i, string s, string e, bool localBrowse = false)
        {
            UpdateViewCount(i, e, localBrowse);

            string img = "http://136.243.19.216:81/" + s + "/" + i;

            byte[] image;
            var contentType = "image/jpeg";
            ImageFormat imgFormat = ImageFormat.Jpeg;
            if (i.ToLower().Contains(".png")) { 
                contentType = "image/png";
                imgFormat = ImageFormat.Png;
            }

            image = new WebClient().DownloadData(img);

            //convert from tiff to png
            var tiffStream = new MemoryStream(image);
            var imgStream = new MemoryStream();
            Bitmap.FromStream(tiffStream).Save(imgStream, imgFormat);

            //return new ImageResult(imgStream, contentType, HttpCacheability.NoCache);
            return File(image, contentType); 
        }

        private void UpdateViewCount(string ean, string ebayname, bool localBrowse)
        {
            if (ean.Contains("_1") && ebayname != null && ebayname != "" && !localBrowse)
            {
                MongoCollection<BsonDocument> Collection = new MongoClient("mongodb://client148:client148devnetworks@136.243.44.111/EAD_" + ebayname)
                .GetServer()
                .GetDatabase("EAD_" + ebayname)
                .GetCollection<BsonDocument>("listing");

                Collection.Update(Query.EQ("Ean", ean.Split('_')[0]), Update.Inc("Views", 1), UpdateFlags.Multi);
            }
        }

        public ActionResult Upload(string i, string f)
        {
            byte[] image = new WebClient().DownloadData(i);

            var contentType = "image/jpeg";
            ImageFormat imgFormat = ImageFormat.Jpeg;
            if (i.ToLower().Contains(".png"))
            {
                contentType = "image/png";
                imgFormat = ImageFormat.Png;
            }


           
            
            string path = System.IO.Path.Combine(
                                       Server.MapPath("~/images/"), f);

            if (!System.IO.File.Exists(path))
            {
                using (System.IO.MemoryStream imageStream = new System.IO.MemoryStream(image))
                {
                    Image img;
                    img = new Bitmap(Image.FromStream(imageStream));
                    img.Save(path);

                }
            }

            


            return RedirectToAction("Index");
        }

        public ActionResult ShowLogo(string i)
        {
            string img = "http://136.243.19.216:81/" + i;

            byte[] image;
            var contentType = "image/jpeg";
            ImageFormat imgFormat = ImageFormat.Jpeg;
            if (i.ToLower().Contains(".png"))
            {
                contentType = "image/png";
                imgFormat = ImageFormat.Png;
            }

            image = new WebClient().DownloadData(img);

            //convert from tiff to png
            var tiffStream = new MemoryStream(image);
            var imgStream = new MemoryStream();
            Bitmap.FromStream(tiffStream).Save(imgStream, imgFormat);

            //return new ImageResult(imgStream, contentType, HttpCacheability.NoCache);
            return File(image, contentType);
        }

        public byte[] GetImageFromServer(string name)
        {
            using (var stream = new FileStream(Server.MapPath(name), FileMode.Open))
            {
                var memoryStream = new MemoryStream();
                stream.CopyTo(memoryStream);
                return memoryStream.ToArray();
            }
        }

        public ActionResult Test(string i)
        {
            ViewBag.Link = i;
            return View();
        }

    }
}
