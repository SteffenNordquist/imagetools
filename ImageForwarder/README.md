# README #

### ImageForwarder ###

* An MVC project deployed to active ebay agent servers. As the name itself, it will forward the image from 136.243.19.216 server to eBay. Telling eBay that the image source is from different server (agent server). ImageForwarder url is being used by ebay listing tool for image urls to present to eBay during listing uploads/revisions.

### How do I get set up? ###

* Ensure IIS is installed in the agent server
* Ensure ASP.NET 3.5/later is installed in the agent server
* Make the project's 'images' folder to allow read/write 
* Deploy same as how BK website is deployed

### Who do I talk to? ###

* Wylan Osorio