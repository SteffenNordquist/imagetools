﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
namespace QuadrantImage
{
    public class Cropper
    {
        private Bitmap imageToProcess = null;

        public Cropper(string sourcepath, string outputpath)
        {
            if (File.Exists(sourcepath))
            {
                FileInfo file = new FileInfo(sourcepath);
                if (file.Extension.ToLower() == ".jpg" || file.Extension.ToLower() == ".png")
                {
                    Image temp = Image.FromFile(file.FullName);
                    using (imageToProcess = new Bitmap(temp))
                    {
                        temp.Dispose();
                        Crop(outputpath);
                    }
                    
                }
                else
                {
                    throw new FormatException();
                }
                
            }
            else
            {
                throw new FileNotFoundException();
            }
        }

        public Cropper(Image sourceimage, string outputpath)
        {

            using (imageToProcess = new Bitmap(sourceimage))
            {
                sourceimage.Dispose();
                Crop(outputpath);
            }
        }

        private void Crop(string outputpath)
        {
            using (Bitmap imgBitmap = new Bitmap(imageToProcess))
            {

                int imagewidth = imgBitmap.Width;
                int imageheight = imgBitmap.Height;

                int outputSize = Math.Max(imagewidth, imageheight);

                using (Bitmap output = new Bitmap(outputSize, outputSize))
                {
                    using (Graphics g = Graphics.FromImage(output))
                    {
                        using (SolidBrush brush = new SolidBrush(Color.White))
                        {
                            g.FillRectangle(brush, 0, 0, outputSize, outputSize);

                            if (imagewidth < imageheight)
                            {
                                g.DrawImage(imgBitmap, (outputSize - imagewidth) / 2, 0, imagewidth, outputSize);
                            }
                            else
                            {
                                g.DrawImage(imgBitmap, 0, (outputSize - imageheight) / 2, outputSize, imageheight);
                            }

                            output.Save(outputpath);
                        }
                    }
                }
            }

            
            
        }
        
    }
}
