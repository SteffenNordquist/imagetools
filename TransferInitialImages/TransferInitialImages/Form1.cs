﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Driver;
using System.IO;
using System.Threading;
using DN_Classes.Agent;
namespace TransferInitialImages
{
    public partial class Form1 : Form
    {

        private string ImagesDirectory;
        private MongoCollection<AgentEntity> AgentsCollection = new MongoClient("mongodb://client148:client148devnetworks@136.243.44.111/Agents")
            .GetServer()
            .GetDatabase("Agents")
            .GetCollection<AgentEntity>("agents");

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            PopulateAgentIds();
        }

        private void PopulateAgentIds()
        {
            foreach (AgentEntity agent in AgentsCollection.FindAll().SetFields("agent.agentID").OrderBy(x=>x.agent.agentID))
            {
                cmbAgentId.Items.Add(agent.agent.agentID);
            }
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                ImagesDirectory = folderBrowserDialog1.SelectedPath;
                txtDirectory.Text = ImagesDirectory;
            }
        }

        private void btnTransfer_Click(object sender, EventArgs e)
        {
            if (cmbAgentId.SelectedIndex > -1)
            {
                if (ImagesDirectory != "" && ImagesDirectory != null)
                {
                    Agent agent = new Agent(int.Parse(cmbAgentId.SelectedItem.ToString()));

                    string supplierName = ImagesDirectory.Split('\\').ToList().Last();

                    DirectoryInfo directory = new DirectoryInfo(ImagesDirectory);
                    List<string> initialImages = new List<string>();
                    directory.GetFiles().ToList().Where(x => x.Name.Contains("_1")).ToList().ForEach(x => initialImages.Add(x.Name));

                    panel1.Enabled = false;

                    Thread thread = new Thread(() => DoTransfer(agent,supplierName,initialImages));
                    thread.Start();

                    while (thread.IsAlive)
                    {
                        Thread.Sleep(1000);
                    }

                    panel1.Enabled = true;

                    
                }
                else
                {
                    MessageBox.Show("Please select images directory!");
                }
            }
            else
            {
                MessageBox.Show("Please select Agent Id");
            }
            
        }

        private void DoTransfer(Agent agent, string supplierName, List<string> initialImages)
        {

            TransferProcessor transferProcessor = new TransferProcessor(agent, supplierName, initialImages);
            transferProcessor.Process();
            
            MessageBox.Show("Finished!");
            
        }
    }
}
