﻿
using DN_Classes.Agent;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TransferInitialImages
{
    public class TransferProcessor
    {
        private Agent Agent;
        private string SupplierName;
        private List<string> InitialImages;
        private StringBuilder sbLog = new StringBuilder();
        private Form1 form;
        private Label label;

        public TransferProcessor(Agent agent, string supplierName, List<string> initialImages)
        {
            this.Agent = agent;
            this.SupplierName = supplierName;
            this.InitialImages = initialImages;
            this.label = label;
            this.form = form;
        }

        public void Process()
        {
            foreach (string initialImage in InitialImages)
            {
                int trials = 0;
                while (trials < 2)
                {

                    try
                    {
                        UploadImage(initialImage);
                        sbLog.AppendLine("success : " + initialImage);
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.Contains("The remote server returned an error: (500) Internal Server Error"))
                        {
                            trials++;
                            Thread.Sleep(3000);
                            
                        }
                        else
                        {
                            trials += 3;
                        }
                        sbLog.AppendLine("failure : " + initialImage + "\t" + ex.Message + "\t" + ex.StackTrace);
                    }
                }
            }


            WriteLog();
        }

        private void UploadImage(string imageName)
        {
            string imagelink = String.Format("http://136.243.19.216:81/{0}/{1}", SupplierName, imageName);
            string uploadlink = String.Format("{0}/Upload?i={1}&f={2}", Agent.agentEntity.agent.htmlgeneratorproperties.imageforwarderlink, imagelink, imageName);

            WebRequest request = HttpWebRequest.Create(uploadlink);
            request.Timeout = 60000;

            var response = request.GetResponse();
            
            response.Dispose();

        }

        private void WriteLog()
        {
            File.WriteAllText("transferLog.txt", sbLog.ToString());

            StringBuilder sb = new StringBuilder();
            sb.AppendLine(InitialImages.Count().ToString());
            InitialImages.ForEach(x => sb.AppendLine(x));
            File.WriteAllText("forTransferLog.txt", sb.ToString());
        }

        
    }
}
