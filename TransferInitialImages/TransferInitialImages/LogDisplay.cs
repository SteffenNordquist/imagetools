﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TransferInitialImages
{
    public partial class LogDisplay : Form
    {
        public LogDisplay()
        {
            InitializeComponent();
        }

        private void LogDisplay_Load(object sender, EventArgs e)
        {

        }

        public void SetLblLogValue(string value)
        {

            if (lblLog.InvokeRequired)
            {
                this.Invoke((MethodInvoker)delegate() { lblLog.Text = value; });
            }
            else
            {
                lblLog.Text = value;
            }
        }
    }
}
