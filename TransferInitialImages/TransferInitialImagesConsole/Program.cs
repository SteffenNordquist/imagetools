﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TransferInitialImagesConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter image folder...");
            string imageFolder = Console.ReadLine();
            string imageFolderLink = String.Format("http://136.243.19.216:81/{0}/", imageFolder);

            Console.WriteLine("Enter agent upload link...");
            Console.WriteLine("ie : http://78.31.64.165/ImageForwarder/Home/Upload");
            string agentUploadLink = Console.ReadLine();

            WebClient webClient = new WebClient();
            string source = webClient.DownloadString(imageFolderLink);

            HtmlDocument document = new HtmlDocument();
            document.LoadHtml(source);

            List<string> imageNames = new List<string>();
            document.DocumentNode.SelectNodes("//pre/a").ToList()
                .ForEach(x => imageNames.Add(x.InnerText));

            imageNames.RemoveAt(0);

            imageNames = imageNames.Where(x => x.Contains("_1.jpg")).ToList();

            List<string> logs = new List<string>();
            List<string> errorLogs = new List<string>();
            foreach (string imageName in imageNames)
            {
                string imagelink = String.Format("http://136.243.19.216:81/{0}/{1}",imageFolder, imageName);
                string uploadlink = String.Format("{0}?i={1}&f={2}",agentUploadLink, imagelink, imageName);
                System.Net.WebRequest request = System.Net.HttpWebRequest.Create(uploadlink);

                request.Timeout = 60000;

                int trialsCounter = 0;
                while (trialsCounter < 4)
                {
                    try
                    {
                        trialsCounter++;
                        var response = request.GetResponse();

                        response.Dispose();

                        Console.WriteLine(imageName);
                        logs.Add(imageName);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("error image uploading, " + imageName);
                        Console.WriteLine(ex.Message);
                        Console.WriteLine("sleeping in 10 seconds");
                        Thread.Sleep(10000);

                        if (trialsCounter == 3)
                        {
                            errorLogs.Add(imageName);
                        }
                    }
                }
            }

            File.WriteAllLines("uploadedImages.txt", logs);
            File.WriteAllLines("unUploadedImages.txt", errorLogs);
             
        }
    }
}
